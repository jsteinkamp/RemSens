## MODIS scale factors
.modis.sf <- list("MCD15A2H" = list("Fpar_500m" = 0.01,
                                    "Lai_500m" = 0.1,
                                    "FparLai_QC" = 1,
                                    "FparExtra_QC" = 1,
                                    "FparStdDev_500m" = 0.01,
                                    "LaiStdDev_500m" = 0.1),
                  "MCD15A3H" = list("Fpar_500m" = 0.01,
                                    "Lai_500m" = 0.1,
                                    "FparLai_QC" = 1,
                                    "FparExtra_QC" = 1,
                                    "FparStdDev_500m" = 0.01,
                                    "LaiStdDev_500m" = 0.1),
                  "MOD17A2H" = list("Gpp_500m" = 0.0001,
                                    "PsnNet_500m" = 0.0001),
                  "MOD17A3H" = list("Npp_500m" = 0.0001),
                  "MOD44B" = list("Percent_Tree_Cover" = 1,
                                  "Percent_NonTree_Vegetation" = 1,
                                  "Percent_Nonvegetated" = 1,
                                  "Quality" = 1,
                                  "Percent_Tree_Cover_SD" = 0.01,
                                  "Percent_Nonvegetated_SD" = 0.01,
                                  "Cloud" = 1),
                  "MOD13C1" = list("CMG 0.05 Deg 16 days NDVI" = 0.0001,
                                   "CMG 0.05 Deg 16 days EVI" = 0.0001,
                                   "CMG 0.05 Deg 16 days VI Quality" = 1,
                                   "CMG 0.05 Deg 16 days red reflectance (Band 1)" = 0.0001,
                                   "CMG 0.05 Deg 16 days NIR reflectance (Band 2)" = 0.0001,
                                   "CMG 0.05 Deg 16 days blue reflectance (Band 3)" = 0.0001,
                                   "CMG 0.05 Deg 16 days MIR reflectance (Band 7)" = 0.0001,
                                   "CMG 0.05 Deg 16 days Avg sun zenith angle" = 0.01,
                                   "CMG 0.05 Deg 16 days NDVI std dev" = 0.0001,
                                   "CMG 0.05 Deg 16 days EVI std dev" = 0.0001,
                                   "CMG 0.05 Deg 16 days #1km pix used" =1,
                                   "CMG 0.05 Deg 16 days #1km pix +-30deg VZ"=1,
                                   "CMG 0.05 Deg 16 days pixel reliability"=1))

## MODIS valid ranges
.modis.vr <- list("MCD15A2H" = list("Fpar_500m" = c(0, 100),
                                    "Lai_500m" = c(0, 100),
                                    "FparLai_QC" = c(0, 254),
                                    "FparExtra_QC" = c(0, 254),
                                    "FparStdDev_500m" = c(0, 100),
                                    "LaiStdDev_500m" = c(0, 100)),
                  "MCD15A3H" = list("Fpar_500m" = c(0, 100),
                                    "Lai_500m" = c(0, 100),
                                    "FparLai_QC" = c(0, 254),
                                    "FparExtra_QC" = c(0, 254),
                                    "FparStdDev_500m" = c(0, 100),
                                    "LaiStdDev_500m" = c(0, 100)),
                  "MOD17A2H" = list("Gpp_500m" = c(0, 3000),
                                    "PsnNet_500m" = c(-3000, 3000),
                                    "PsnNet_QC_500m" = c(0, 240)),
                  "MOD17A3H" = list("Npp_500m" = c(-3000, 32700),
                                    "Npp_QC_500m" = c(0, 254)),
                  "MOD44B" = list("Percent_Tree_Cover" = c(0, 100),
                                  "Percent_NonTree_Vegetation" = c(0, 100),
                                  "Percent_NonVegetated" = c(0, 100),
                                  "Quality" = c(0, 255),
                                  "Percent_Tree_Cover_SD" = c(0, 10000),
                                  "Percent_Nonvegetated_SD" = c(0, 10000),
                                  "Cloud" = c(0, 255)),
                  "MOD13C1" = list("CMG 0.05 Deg 16 days NDVI" = c(-2000, 10000),
                                   "CMG 0.05 Deg 16 days EVI" = c(-2000, 10000),
                                   "CMG 0.05 Deg 16 days VI Quality" = c(0, 65534),
                                   "CMG 0.05 Deg 16 days red reflectance (Band 1)" = c(0, 10000),
                                   "CMG 0.05 Deg 16 days NIR reflectance (Band 2)" = c(0, 10000),
                                   "CMG 0.05 Deg 16 days blue reflectance (Band 3)" = c(0, 10000),
                                   "CMG 0.05 Deg 16 days MIR reflectance (Band 7)" = c(0, 10000),
                                   "CMG 0.05 Deg 16 days Avg sun zenith angle" = c(-9000, 9000),
                                   "CMG 0.05 Deg 16 days NDVI std dev" = c(-2000, 10000),
                                   "CMG 0.05 Deg 16 days EVI std dev" = c(-2000, 10000),
                                   "CMG 0.05 Deg 16 days #1km pix used" = c(0, 26),
                                   "CMG 0.05 Deg 16 days #1km pix +-30deg VZ" = c(0,36),
                                   "CMG 0.05 Deg 16 days pixel reliability" = c(0,4)))


#' Retrieve the scaling_factor or valid_range for a MODIS product band
#'
#' @param p MODIS product string
#' @param b MODIS product band name
#' @param what should the s[caling_factor] or v[alid_range] be returned.
#'
#' @return the respective scaling_factor or valid_range
#' @export
#'
#' @examples
#' getMODISscaling("MOD17A3H", "Npp_500m")
#' getMODISscaling("MOD17A3H", "Npp_500m", "v")
getMODISscaling <- function(p, b, what="s") {
  if (grepl("s", tolower(what))) {
    if (!is.null(.modis.sf[[p]][[b]])) {
      return(.modis.sf[[p]][[b]])
    } else {
      warning("scale_factor not known, returning 1.")
      return(1)
    }
  } else if (grepl("v", tolower(what))) {
    if (!is.null(.modis.vr[[p]][[b]])) {
      return(.modis.vr[[p]][[b]])
    } else {
      warning("valid_range not known, returning -Inf/Inf.")
      return(c(-Inf, Inf))
    }
  } else {
    stop(paste0("Don't know what you want ('", what, "'), pass either 's[cale_factor]' or 'v[alid_range]'!"))
  }
}
