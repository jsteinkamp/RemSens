## MODIS landcover classification names (MOD12)
.mod12Names <- data.frame(id=c(0:16, 254, 255),
                           IGBP=c("Water",
                                  "Evergreen Needleleaf forest",
                                  "Evergreen Broadleaf forest",
                                  "Deciduous Needleleaf forest",
                                  "Deciduous Broadleaf forest",
                                  "Mixed forest",
                                  "Closed shrublands",
                                  "Open shrublands",
                                  "Woody savannas",
                                  "Savannas",
                                  "Grasslands",
                                  "Permanent wetlands",
                                  "Croplands",
                                  "Urban and built-up",
                                  "Cropland/Natural vegetation mosaic",
                                  "Snow and ice",
                                  "Barren or sparsely vegetated",
                                  "Unclassified",
                                  "Fill Value"),
                           UMD=c("Water",
                                 "Evergreen Needleleaf forest",
                                 "Evergreen Broadleaf forest",
                                 "Deciduous Needleleaf forest",
                                 "Deciduous Broadleaf forest",
                                 "Mixed forest",
                                 "Closed shrublands",
                                 "Open shrublands",
                                 "Woody savannas",
                                 "Savannas",
                                 "Grasslands",
                                 "Croplands",
                                 "Urban and built-up",
                                 "Barren or sparsely vegetated",
                                 NA,
                                 NA,
                                 NA,
                                 "Unclassified",
                                 "Fill Value"),
                           LAI.fPAR=c("Water",
                                      "Grasses/Cereal crops",
                                      "Shrubs",
                                      "Broad-leaf crops",
                                      "Savanna",
                                      "Evergreen Broadleaf forest",
                                      "Deciduous Broadleaf forest",
                                      "Evergreen Needleleaf forest",
                                      "Deciduous Needleleaf forest",
                                      "Non-vegetated",
                                      "Urban",
                                      NA,
                                      NA,
                                      NA,
                                      NA,
                                      NA,
                                      NA,
                                      "Unclassified",
                                      "Fill Value"),
                           stringsAsFactors=FALSE)

#' Translate the numeric landcover type to names
#'
#' The MOD12* products are land cover classifications. Here the remapping of numeric values to names is performed. The functions takes a vector of numbers and returns the respective names.
#'
#' @param x any numeric data that can be converted to a vector
#' @param lct.def landcover type definition to use. Either "IGBP", "UMD" or "LAI.fPAR".
#' @param stringsAsFactors should the names be returned as factor. Defaults to options("stringsAsFactors").
#'
#' @return vector of landcover names
#' @export
#'
#' @examples
#' mod12ID2Name(sample(c(1:16, 254, 255), 25, replace=TRUE), "IGBP")
mod12ID2Name <- function(x, lct.def="LAI.fPAR", stringsAsFactors=options("stringsAsFactors")$stringsAsFactors) {
  x.id <- sapply(x, function(x) {which(.mod12Names$id==x)})
  x.names <- .mod12Names[x.id, lct.def]
  if (stringsAsFactors) {
    x.levels <- .mod12Names[sort(unique(x.id)), lct.def]
    x.names = factor(x.names, levels=x.levels)
  }
  return(x.names)
}
