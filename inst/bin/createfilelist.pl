#!/usr/bin/env perl

use strict;
use warnings;
use WebService::MODIS;
use Getopt::Long;

my $product = "";
my $version = "006";
my @dates;
my @h;
my @v;
my $init = 0;

GetOptions("product=s"    => \$product,
           "version=s"    => \$version,
           "dates=s"      => \@dates,
           "horizontal=s" => \@h,
           "vertical=s"   => \@v,
           "init!"        => \$init)
    or die("Error in command line arguments\n");
@dates = split(/,/,join(',',@dates));
@h = split(/,/,join(',',@h));
@v = split(/,/,join(',',@v));

my $nd = @dates;
@dates = ("2000-01-01", "2022-12-31") if ($nd == 0);
my $nh = @h;
@h = (0, 35) if ($nh == 0);
my $nv = @v;
@v = (0, 17) if ($nv == 0);

## print "$_ " foreach @dates;
## exit;
## print "\n$version\n";
## print "$init\n";


if ($init) {
  print "Reloading cache. Takes ages!\n";
  initCache;
  writeCache;
} else {
  readCache;
}
my $modis = WebService::MODIS->new(product => $product,
                                   version => $version,
                                   dates => \@dates,
                                   h => \@h, v=> \@v);
## $modis->version($version);

$modis->createUrl;
print "$_\n" foreach $modis->url;
