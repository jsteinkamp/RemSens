This is a very simple package for downscaling MODIS satellite products to a spatial resolution on 0.5° so far.

## Workflow:

1. Create and/or go to the directory where to save the MODIS data.

2. To create the URL list of all files the provided perl script `createfilelist.pl`
   can be used. The file can be found with `system.file("bin/createfilelist.pl", package="RemSens")`
   in R. A propper [Perl](https://www.perl.org/) installation with the module
   [WebService::MODIS](https://metacpan.org/pod/WebService::MODIS) is needed. Sadly
   download only works with a registered password only since a while and I have not yet
   updated the Perl code to work with a password. So a detour via wget must be taken.
```
createfilelist.pl -p <product> --version <version> > filelist.txt
```
    
3. Use wget to download the files (Needs a NASA Earthdata ID and password via https://earthdata.nasa.gov/).
   The password for wget can be stored in the file ~/.netrc on unix systems with the line:
```
machine urs.earthdata.nasa.gov login <name> password <password>
```
Keep in mind to `chmod 600 ~/.netrc`. Then the part '--user=&lt;username&gt; --ask-password' can be omitted.
```
wget -c -i filelist.txt --user=<username> --ask-password
```

4. Reproject each v-band to the resolution of 0.5°, combine them and save them as data.table
   with the R script `reproject.R`,  which
   again can be found via R: `system.file("bin/reproject.R", package="RemSens")`
```
reproject.R -p <product> -v <version> -y <year1>[,<year2>,...]
```
